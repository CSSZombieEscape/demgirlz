
Populate player name (from most used one) after database is fully populated:
```SQL
UPDATE `player` SET `name`=(
    SELECT `name`
    FROM `player_names`
    WHERE `player_names`.guid = `player`.guid
    GROUP BY guid, name
    ORDER BY MAX(`time`) DESC LIMIT 1
);
```
