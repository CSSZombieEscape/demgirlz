# pylint: disable=line-too-long

SQLALCHEMY_DATABASE_URI = 'mysql://demweb:demweb@127.0.0.1:3306/demweb?charset=utf8mb4'
SQLALCHEMY_TRACK_MODIFICATIONS = False
