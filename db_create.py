#!/usr/bin/env python3
from demweb.app import create_app
from demweb.extensions import db

if __name__ == '__main__':
    app = create_app()
    with app.app_context():
        db.create_all()
        db.session.commit()
