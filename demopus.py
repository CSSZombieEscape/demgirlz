import struct
import sys

with open(sys.argv[1], 'rb') as fp:
	data = fp.read()

ofs = 16
counter = 0

while ofs < len(data):
	header = struct.unpack('B', data[ofs:ofs+1])[0]
	ofs += 1
	print(f'#{ofs} Header: {header}')

	if header == 0x01:
		srate = struct.unpack('I', data[ofs:ofs+4])[0]
		ofs += 4
		samples = struct.unpack('Q', data[ofs:ofs+8])[0]
		ofs += 8
		print(f'srate: {srate} | samples: {samples}\n')
	elif header == 0x02:
		dlen = struct.unpack('Q', data[ofs:ofs+8])[0]
		ofs += 8
		with open(f'{counter}.opus', 'wb') as fp:
			fp.write(data[ofs:ofs+dlen])
		counter += 1
		ofs += dlen
		print(f'opus len {dlen}')
	elif header == 0x03:
		silence = struct.unpack('Q', data[ofs:ofs+8])[0]
		ofs += 8
		print(f'silence {silence}')
	elif header == 0x04:
		break
