from flask_sqlalchemy import SQLAlchemy
from dictalchemy import make_class_dictable

import config

db = SQLAlchemy()
make_class_dictable(db.Model)

default_config = dict()
for key in dir(config):
    if key.isupper():
        default_config[key] = getattr(config, key)
