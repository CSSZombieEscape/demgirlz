# pylint: disable=no-member
from flask import current_app

from sqlalchemy.types import JSON

from demweb.extensions import db


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, index=True)
    length = db.Column(db.Float, index=True)
    demoname = db.Column(db.String(255), unique=True)
    mapname = db.Column(db.String(255))
    mapmd5 = db.Column(db.String(32))
    servername = db.Column(db.String(255))
    frames = db.Column(db.Integer)
    ticks = db.Column(db.Integer)
    tickinterval = db.Column(db.Float)
    dirty = db.Column(db.Boolean)

    playtime = db.Column(db.Float, index=True)
    rounds = db.Column(db.Integer, index=True)
    ct_wins = db.Column(db.Integer, index=True)
    t_wins = db.Column(db.Integer, index=True)
    chats = db.Column(db.Integer, index=True)
    deaths = db.Column(db.Integer, index=True)
    kills = db.Column(db.Integer, index=True)
    voice_active = db.Column(db.Float, index=True)
    voice_total = db.Column(db.Float, index=True)
    silence_chunks = db.Column(db.JSON(65535))


class Player(db.Model):
    guid = db.Column(db.String(32), primary_key=True)
    name = db.Column(db.String(32), primary_key=True)
    first_seen = db.Column(db.DateTime, index=True)
    last_seen = db.Column(db.DateTime, index=True)
    playtime = db.Column(db.Float, index=True)
    chats = db.Column(db.Integer, index=True)
    deaths = db.Column(db.Integer, index=True)
    kills = db.Column(db.Integer, index=True)
    voicetime = db.Column(db.Float, index=True)


class PlayerNames(db.Model):
    guid = db.Column(db.String(32), db.ForeignKey('player.guid'), primary_key=True)
    name = db.Column(db.String(32), primary_key=True)
    time = db.Column(db.Float)

    player = db.relationship('Player', backref='names', foreign_keys=[guid])


class PlayerSprays(db.Model):
    guid = db.Column(db.String(32), db.ForeignKey('player.guid'), primary_key=True)
    spray = db.Column(db.String(32), primary_key=True)

    player = db.relationship('Player', backref='sprays', foreign_keys=[guid])


class PlayerSession(db.Model):
    player_guid = db.Column(db.String(32), db.ForeignKey('player.guid'), primary_key=True)
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'), primary_key=True)
    playtime = db.Column(db.Float)
    chats = db.Column(db.Integer)
    deaths = db.Column(db.Integer)
    kills = db.Column(db.Integer)
    voicetime = db.Column(db.Float)

    session = db.relationship('Session', foreign_keys=[session_id])
    player = db.relationship('Player', backref='sessions', foreign_keys=[player_guid])


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    player_guid = db.Column(db.String(32), db.ForeignKey('player.guid'))
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'))
    tick = db.Column(db.Integer)
    time = db.Column(db.DateTime, index=True)
    name = db.Column(db.String(255))
    chat = db.Column(db.String(1024))


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    player_guid = db.Column(db.String(32), db.ForeignKey('player.guid'))
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'))
    tick = db.Column(db.Integer)
    time = db.Column(db.DateTime, index=True)
    event = db.Column(db.String(32), index=True)
    data = db.Column(db.JSON(1024))

