from flask import Blueprint, render_template

from demweb.extensions import db
from demweb.models import *

bp = Blueprint('views', __name__)

@bp.route('')
def home():
    return 'hello world'

@bp.route('/session/<int:session_id>')
def session(session_id):
    session = Session.query.filter_by(id=session_id).one()
    if not session:
        return '404 Not Found', 404

    player_sessions_ = PlayerSession.query.filter_by(session_id=session_id).all()
    player_sessions = dict()
    for psess in player_sessions_:
        player_sessions[psess.player_guid] = psess
    del player_sessions_

    players_ = Player.query.filter(Player.guid.in_(player_sessions.keys()))
    players = dict()
    for player in players_:
        players[player.guid] = player
    del players_

    chats = Chat.query.filter_by(session_id=session_id).all()
    chats.sort(key=lambda x: x.tick)

    events = Event.query.filter_by(session_id=session_id).all()
    events.sort(key=lambda x: x.tick)

    return render_template('session.html',
        session=session,
        player_sessions=player_sessions,
        players=players,
        chats=chats,
        events=events
    )

@bp.route('/player/<guid>')
def player(guid):
    player = Player.query.filter_by(guid=guid).one()
    if not player:
        return '404 Not Found', 404

    player_sessions = PlayerSession.query.filter_by(player_guid=guid).all()

    return render_template('player.html',
        player=player,
        player_sessions=player_sessions
    )
